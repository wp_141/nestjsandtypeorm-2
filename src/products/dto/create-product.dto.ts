import { IsNotEmpty, Length, IsPositive } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(2, 100)
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;
}
